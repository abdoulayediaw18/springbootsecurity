package com.example.springsecumysql.services;

import com.example.springsecumysql.config.CustomUsersDetails;
import com.example.springsecumysql.config.CustomUsersDetailsService;
import com.example.springsecumysql.entities.AuthenticationRequest;
import com.example.springsecumysql.entities.AuthenticationResponse;
import com.example.springsecumysql.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRessource {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    CustomUsersDetailsService customUsersDetailsService;

    @Autowired
    JwtUtils jwtUtils;

    @GetMapping("/hello")
    public String hello(){
        return "hello mr Diaw";
    }


    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
      try {
          authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));

      }
      catch (BadCredentialsException e){


      }

        UserDetails userDetails= customUsersDetailsService.loadUserByUsername(authenticationRequest.getUsername());

      String jwt= jwtUtils.generateToken(userDetails);

        AuthenticationResponse authenticationResponse=new AuthenticationResponse(jwt);

       return ResponseEntity.ok(authenticationResponse);

    }

}
