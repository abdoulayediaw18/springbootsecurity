package com.example.springsecumysql.services;

import com.example.springsecumysql.dao.UsersRepository;
import com.example.springsecumysql.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/users")



public class UsersRessource {

    @Autowired
    UsersRepository usersRepository;

    @GetMapping("/all")
    public List<Users> getAll(){
       return usersRepository.findAll();
    }

    @PostMapping("/load")
    public List<Users> persist(@RequestBody Users users){
        usersRepository.save(users);
        return  usersRepository.findAll();

    }
}
