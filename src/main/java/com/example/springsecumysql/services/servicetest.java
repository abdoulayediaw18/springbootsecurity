package com.example.springsecumysql.services;

import com.example.springsecumysql.dao.PersonneDao;
import com.example.springsecumysql.entities.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@RestController()
@RequestMapping(path="/personne",  produces =MediaType.APPLICATION_JSON_UTF8_VALUE )


public class servicetest {
    @Autowired
    PersonneDao personneDao;


    @GetMapping("/testme")
    public Response testme(){

        return Response.ok( "okk you'r in").build();
    }


    @GetMapping("/testme2")
    public String testme2(){
        return "The 2 rocks";
    }


    @PostMapping(path="/add",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void addPersonne(@RequestBody  Personne personne){
        personne.setDatenaiss(new Date());
        personneDao.save(personne);
    }

    @PostMapping("/delete")
    public void delPersonne(Long id){
        personneDao.delete(id);
    }


    @PostMapping(path="/update",consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updatePersonne(Personne personne){
       personneDao.save(personne);
    }

    @GetMapping("/getOne/{id}")
    public Personne getPersonne( @PathVariable String id){
       return personneDao.findOne(new Long(id));
    }


   // @Secured(value="{POS}")
    @GetMapping("/getAll")
    public List<Personne> getAllPersonne(){
     return   personneDao.findAll();
    }

}
