package com.example.springsecumysql.dao;


import com.example.springsecumysql.entities.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneDao extends JpaRepository<Personne,Long> {
}
