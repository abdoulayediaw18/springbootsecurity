package com.example.springsecumysql.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Users")
public class Users implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="user_id")
    private Integer id;
    @Column(name="email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "name")
    private String name;
    @Column(name="last_name")
    private String lastName;
    @Column(name = "active")
    private int active;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",joinColumns = @JoinColumn(name="user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public Users(Users users){
        this.active=users.getActive();
        this.email=users.getEmail();
        this.password=users.getPassword();
        this.name=users.getName();
        this.lastName=users.getLastName();
        this.roles=users.getRoles();
        this.id=users.getId();
    }


}
