package com.example.springsecumysql.config;

import com.example.springsecumysql.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Service
public class JwtRequestFilter extends OncePerRequestFilter {

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    CustomUsersDetailsService customUsersDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
      String authorisationHeader = httpServletRequest.getHeader("Authorization");

      String username=null;
      String jwt=null;

      if(authorisationHeader!=null && authorisationHeader.startsWith("Bearer ")) {
           jwt=authorisationHeader.substring(7);
          username=  jwtUtils.extractUsername(jwt);
      }

      if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null){
          UserDetails userDetails= customUsersDetailsService.loadUserByUsername(username);

         if(jwtUtils.validateToken(jwt,userDetails)){
             UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken= new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
             usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
             SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
         }
      }
      filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
