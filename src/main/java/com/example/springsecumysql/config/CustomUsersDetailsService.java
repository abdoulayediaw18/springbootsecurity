package com.example.springsecumysql.config;

import com.example.springsecumysql.dao.UsersRepository;
import com.example.springsecumysql.entities.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class CustomUsersDetailsService implements UserDetailsService {

    @Autowired
    UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       /* Optional<Users> optionalUsers=usersRepository.findByName(username);
        optionalUsers.
                orElseThrow(()->new UsernameNotFoundException("username not found"));
       return optionalUsers.map( CustomUsersDetails::new).get();*/
       Users users=usersRepository.findByName(username).get();
       if (users==null) throw new UsernameNotFoundException("Username not found");
       //else
        Collection<GrantedAuthority> authorities= new ArrayList<>();
        users.getRoles().forEach(r -> {
            authorities.add(new SimpleGrantedAuthority(r.getRole()));
        });

        return new User(users.getName(),users.getPassword(),authorities);
    }
}
