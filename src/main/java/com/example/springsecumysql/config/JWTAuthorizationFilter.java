package com.example.springsecumysql.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class JWTAuthorizationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        System.out.println("************************ Inside JWTAuthorizationFilter");
        httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
        httpServletResponse.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,authorization");
        httpServletResponse.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, authorization");

        if(httpServletRequest.getMethod().equals("OPTIONS")){
            System.out.println("Inside Option");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
             httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
            httpServletResponse.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,authorization");
            httpServletResponse.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, authorization");
           doFilter(httpServletRequest,httpServletResponse,filterChain);
        }

        String jwtToken= httpServletRequest.getHeader("authorization");

        if(jwtToken==null){
            filterChain.doFilter(httpServletRequest,httpServletResponse);
        }

         System.out.println(jwtToken);
        jwtToken=jwtToken.replace("Bearer ","");
        Claims claims= Jwts.parser()
                           .setSigningKey("secret")
                            .parseClaimsJws(jwtToken)
                            .getBody();
        String username=claims.getSubject();
        ArrayList<Map<String,String>> roles=(ArrayList<Map<String, String>>) claims.get("roles");
        Collection<GrantedAuthority> authorities=new ArrayList<>();
        roles.forEach(r->{
            authorities.add(new SimpleGrantedAuthority(r.get("authority")));
        });

        UsernamePasswordAuthenticationToken authenticatedUser= new UsernamePasswordAuthenticationToken(username,null,authorities);
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
        filterChain.doFilter(httpServletRequest,httpServletResponse);
        System.out.println("OncePerRequestFilter: "+username);
    }
}
