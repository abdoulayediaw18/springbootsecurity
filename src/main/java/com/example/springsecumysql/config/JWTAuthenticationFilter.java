package com.example.springsecumysql.config;

import com.example.springsecumysql.entities.Users;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    AuthenticationManager authenticationManager;
    String password=null;

    JWTAuthenticationFilter(AuthenticationManager authenticationManager){
        super();
        this.authenticationManager=authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Users users=null;
        try {
             users = new ObjectMapper().readValue(request.getInputStream(), Users.class);
             this.password=users.getPassword();
        } catch(Exception e){
            throw new RuntimeException(e);
        }
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(users.getName(),users.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        User springUser=new User(authResult.getName(),this.password,authResult.getAuthorities());


        String jwtToken= Jwts.builder()
                            .setSubject(springUser.getUsername())
                            .setExpiration(new Date(System.currentTimeMillis()+864_000_000))
                            .signWith(SignatureAlgorithm.HS256,"secret")
                            .claim("roles",springUser.getAuthorities())
                            .claim("ressources","tableau de ressources")
                            .compact();
        response.addHeader("Authorization","Bearer "+jwtToken);
        System.out.println("Token: "+jwtToken);
    }
}
