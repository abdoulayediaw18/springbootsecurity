package com.example.springsecumysql.config;

import com.example.springsecumysql.dao.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;



@Configuration
@EnableWebSecurity
    @EnableJpaRepositories(basePackageClasses = UsersRepository.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUsersDetailsService usersDetailsService;

    @Autowired
    JwtRequestFilter jwtRequestFilter;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    public void globalConfig(AuthenticationManagerBuilder auth) throws Exception{
       auth.userDetailsService(usersDetailsService).passwordEncoder(getPasswordEncoder());
       //auth.userDetailsService(usersDetailsService).passwordEncoder(bCryptPasswordEncoder);
        //auth.inMemoryAuthentication().withUser("admin").password("pass").roles("ADMIN");

    }


 public void configure(HttpSecurity http) throws  Exception{
     //   http.authorizeRequests().anyRequest().authenticated().and().httpBasic();
           //     .and().antMatcher("**/testme").authorizeRequests().anyRequest().permitAll().and().httpBasic() ;


     http
             .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
             .and()
             .csrf().disable()
             .authorizeRequests()
             .antMatchers(HttpMethod.POST,"/authenticate")
             .permitAll()
             .antMatchers("/personne/testme2")
             .permitAll()
            // .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
             .anyRequest()
             .authenticated()
          //   .and()
            // .formLogin()
           //  .and()
         //    .httpBasic()
         //    .and()
         //    .addFilter(new JWTAuthenticationFilter(authenticationManager()))
          //   .addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
               .and().addFilterBefore(jwtRequestFilter,UsernamePasswordAuthenticationFilter.class)
             ;


   //  http  .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable().authorizeRequests().antMatchers("/authenticate").permitAll().anyRequest().authenticated();




 }

    private PasswordEncoder getPasswordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence charSequence) {
                return charSequence.toString();
            }

            @Override
            public boolean matches(CharSequence charSequence, String s) {
                return charSequence.toString().equals(s);
               // return true;
            }
        };
    }


}
