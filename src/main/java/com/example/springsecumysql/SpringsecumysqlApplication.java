package com.example.springsecumysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringsecumysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringsecumysqlApplication.class, args);
    }


  @Bean
    BCryptPasswordEncoder getBCPE(){
        return new BCryptPasswordEncoder();
  }
}
